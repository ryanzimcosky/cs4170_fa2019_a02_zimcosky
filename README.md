# OpenMP Assignment 02 - ZIMCOSKY

## Summary of class

Since class started we have been learning about data oriented parallel programming with OpenMP. This language/macro has many benificial tools, but it seems like my favorite is the parallel for clause. The idea behind OpenMP is that when omp parallel is called, a set amount of threads are made, thirsty to handle some data. After that the many clauses can help you tell those threads how and how not to handle the following chunk of code.

## Summary of my work

I parallelized the PSO algorithm by first moving the timers around to see where the program spent the most time. Then, I essentially just used different variatiations of the for clause to parallelize the the more painful loops in the parts i needed to speed up

## Results
My parallel algorithms to find the position became faster as the problem size increased, and the others were generally only dependent on the number of threads, but were also faster 

### Tables & Graphs
You need to include tables, charts, etc. to demonstrate time, speedup, efficiency, and karp-flatt metric


### OpenMP Assignment #2 for CS 4170/5170 ###
This is a basic template for OpenMP Assignment in CS 4170/5170

## Starting your own Repo ##

Follow these steps:

1. Create your appropriately named repo on Gitlab.

2. On your local computer: 
    - if using SSH, run  `git clone -b Assignment2 --single-branch git@gitlab.com:rgreen13/openmp.git <DIRECTORY_NAME>`. 
    - If using HTTPS, run `git clone -b Assignment2 --single-branch https://gitlab.com/rgreen13/openmp.git <DIRECTORY_NAME>`

3. `cd <DIRECTORY_NAME>`

4. `git remote remove origin`. This removes the current remote named `origin`.

5. `git remote add origin <URL_OF_YOUR_REPO>`. This adds a new remote that has the address of your repo.

6. `git branch -m master`. This changes the name of the current branch (Assignment2) to master.

6. `git push -u origin master`. This pushes your changes.

## Running ##
To compile and run from command line if you are not on windows:
```
cd src
g++ -fopenmp main.cpp CStopWatch.cpp
./a.out
```
or
```
cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp ./OpenMP
```

## Using OSC ##
Move all the files to the Ohio Supercomputing Center (OSC) server of your choice. Make sure to build your code, then modify the `jobScript` accordingly. Submit from inside the `Default` directory using 
```
qsub jobScript
```

You may also do this using http://ondemand.osc.edu